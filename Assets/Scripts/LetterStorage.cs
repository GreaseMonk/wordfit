﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LetterStorage 
{
    private Dictionary<char, DictValue> storedCharacters;

    private void InitializeStoredLetters()
    {
        // TODO: add persistence;

        storedCharacters = new Dictionary<char, DictValue>();

        storedCharacters.Add('a', new DictValue(50));
        storedCharacters.Add('b', new DictValue(50));
        storedCharacters.Add('c', new DictValue(50));
        storedCharacters.Add('d', new DictValue(50));
        storedCharacters.Add('e', new DictValue(50));
        storedCharacters.Add('f', new DictValue(50));
        storedCharacters.Add('g', new DictValue(50));
        storedCharacters.Add('h', new DictValue(50));
        storedCharacters.Add('i', new DictValue(50));
        storedCharacters.Add('j', new DictValue(50));
        storedCharacters.Add('k', new DictValue(50));
        storedCharacters.Add('l', new DictValue(50));
        storedCharacters.Add('m', new DictValue(50));
        storedCharacters.Add('n', new DictValue(50));
        storedCharacters.Add('o', new DictValue(50));
        storedCharacters.Add('p', new DictValue(50));
        storedCharacters.Add('q', new DictValue(50));
        storedCharacters.Add('r', new DictValue(50));
        storedCharacters.Add('s', new DictValue(50));
        storedCharacters.Add('t', new DictValue(50));
        storedCharacters.Add('u', new DictValue(50));
        storedCharacters.Add('v', new DictValue(50));
        storedCharacters.Add('w', new DictValue(50));
        storedCharacters.Add('x', new DictValue(50));
        storedCharacters.Add('y', new DictValue(50));
        storedCharacters.Add('z', new DictValue(50));
    }

    /// <summary>
    /// Retrieves the amount of characters left
    /// </summary>
    /// <param name="character">The character to check the stored amount of</param>
    /// <returns></returns>
    public int GetStoredLettersAmount(char character)
    {
        if (storedCharacters == null)
            InitializeStoredLetters();
        if (!storedCharacters.ContainsKey(character))
            return -1;

        foreach(KeyValuePair<char, DictValue> kvp in storedCharacters)
        {
            if (kvp.Key == character)
                return kvp.Value.iValue;
        }

        return -1;
    }

    public void Substract(char character)
    {
        if (storedCharacters == null)
            InitializeStoredLetters();
        if (!storedCharacters.ContainsKey(character))
            return;

        //KeyValuePair<char, int> newPair = new KeyValuePair<char, int>() { character, GetStoredCharactersAmount(character) - 1 };

        foreach (KeyValuePair<char, DictValue> kvp in storedCharacters)
        {
            if (kvp.Key == character && kvp.Value.iValue > 0)
            {
                kvp.Value.iValue--;
            }
        }
    }

    public void Add(char character)
    {
        if (storedCharacters == null)
            InitializeStoredLetters();
        if (!storedCharacters.ContainsKey(character))
            return;

        //KeyValuePair<char, int> newPair = new KeyValuePair<char, int>() { character, GetStoredCharactersAmount(character) - 1 };

        foreach (KeyValuePair<char, DictValue> kvp in storedCharacters)
        {
            if (kvp.Key == character)
                kvp.Value.iValue++;

        }
    }
}

public class DictValue
{
    public DictValue(int v) { iValue = v; }
    public int iValue;
} 
