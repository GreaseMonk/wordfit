﻿using UnityEngine;
using System.Collections;

public class CreditsManager : MonoBehaviour
{
    public UILabel creditsLabel;
    private int credits;
    private const string creditsPrefix = "";

    public int[] letterBonus;

    private void Start()
    {
        Reset();
        UpdateLabel();
    }

    public void Add(int amount)
    {
        credits += amount;

        UpdateLabel();
    }

    public void Substract(int amount)
    {
        credits = Mathf.Clamp(credits - amount, 0, 99999999);

        UpdateLabel();
    }

    public void Reset()
    {
        credits = 4000;
    }

    private void UpdateLabel()
    {
        creditsLabel.text = creditsPrefix + credits.ToString();
    }

    public int GetReward(string rewardWord)
    {
        int totalReward = letterBonus[rewardWord.Length];
        return totalReward;
    }

    public void SetCreditsLabel(string newText)
    {
        creditsLabel.text = newText;
    }

    public int GetCurrent()
    {
        return credits;
    }
}
