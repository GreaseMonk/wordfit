﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour 
{
    public Vector3 boundsMin, boundsMax;

    [Range(0.1f, 10f)]
    public float speed = 0.5f;

    private GameManager gameManager;
    private Vector2 lastTouchPosition = Vector3.zero;



	// Use this for initialization
	void Start () 
    {
        gameManager = GameManager.instance;
	}
	
	// Update is called once per frame
	void Update () 
    {
        // Don't move the camera when we are typing a word.
        if (gameManager.wordInput.visible)
            return;
        /*if (Input.touchCount == 1)
            ProcessMove(Input.touches[0]);*/

        if (Input.touchCount > 1 && Input.GetTouch(0).phase == TouchPhase.Moved)
        {
            // Clear the current tile selection because we want to move the camera.
            gameManager.tileSelection.ClearSelection();

            Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
            transform.Translate(-touchDeltaPosition.x * speed * Time.deltaTime, -touchDeltaPosition.y * speed * Time.deltaTime, 0);

            transform.position = new Vector3(
                Mathf.Clamp(transform.position.x, boundsMin.x, boundsMax.x),
                Mathf.Clamp(transform.position.y, boundsMax.y, boundsMin.y),
                transform.position.z
                );
        }
	}

    private void ProcessMove(Touch touch)
    {
        Vector3 currentPosition = Camera.main.transform.position;
        Vector3 nextPosition = Vector3.zero;

        Vector2 positionChange = lastTouchPosition - touch.position;

        positionChange = new Vector2(
            positionChange.x,
            Screen.width - positionChange.y);

        positionChange *= Time.deltaTime * speed;

        nextPosition = new Vector3(
            Mathf.Clamp(currentPosition.x + positionChange.x, boundsMin.x, boundsMax.x), 
            Mathf.Clamp(currentPosition.y + positionChange.y, boundsMin.y, boundsMax.y), 
            currentPosition.z);


        nextPosition = Vector3.Lerp(currentPosition, nextPosition, Time.deltaTime * speed);

        Camera.main.transform.position = nextPosition;

        lastTouchPosition = touch.position;
    }
}
