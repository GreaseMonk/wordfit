﻿using UnityEngine;
using System.Collections;

public class ZoomHandler : MonoBehaviour 
{
    private Vector3 defaultPosition;
    // private Vector3 defaultScale;
    // private Quaternion defaultRotation;

    public AnimationCurve curve;

    private GameTile previousTile;

    private bool zooming = false;

	/// <summary>
	/// Executed on startup.
	/// </summary>
	private void Start () 
    {
        defaultPosition = transform.position;
        // defaultScale = transform.localScale;
        // defaultRotation = transform.rotation;
	}

    public void ZoomTo(GameTile tile)
    {
        if(zooming)
        {
            StopCoroutine("ZoomRoutine");
        }

        StartCoroutine(ZoomRoutine(tile));
    }

    public IEnumerator ZoomRoutine(GameTile tile)
    {
        zooming = true;
        float startTime = Time.time;
        float duration = 1f;
        float currentTime;

        if (previousTile != null)
            previousTile.GlowYellow(false);

        previousTile = tile;
        tile.GlowYellow(true);

        Vector3 oldPosition = transform.position;
        Vector3 targetPosition = new Vector3(tile.transform.position.x, tile.transform.position.y - 3f, -30f);

        while ((currentTime = (Time.time - startTime)) < duration)
        {
            Vector3 newPosition = Vector3.Lerp(oldPosition, targetPosition, curve.Evaluate(currentTime));
            transform.position = newPosition;

            yield return null;
        }

        transform.position = targetPosition;

        zooming = false;
    }

    public void Reset()
    {
        if (zooming)
        {
            StopCoroutine("ResetRoutine");
        }
        
        StartCoroutine(ResetRoutine());
    }

    public IEnumerator ResetRoutine()
    {
        zooming = true;
        float startTime = Time.time;
        float duration = 1f;
        float currentTime;

        Vector3 oldPosition = transform.position;
        Vector3 targetPosition = defaultPosition;

        previousTile.GlowYellow(false);

        while ((currentTime = (Time.time - startTime)) < duration)
        {
            Vector3 newPosition = Vector3.Lerp(oldPosition, targetPosition, curve.Evaluate(currentTime));
            transform.position = newPosition;

            yield return null;
        }

        transform.position = targetPosition;

        zooming = false;
    }
}
