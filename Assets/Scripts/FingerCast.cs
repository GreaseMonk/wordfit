﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Casts a ray into the game scene from the user's finger, down onto the cubes underneath so that 
/// cubes may be selected and chosen to fill in words.
/// </summary>
public class FingerCast : MonoBehaviour 
{
    public GameManager gameManager;
    public TutorialScreen tutorialScreen;

    private const float TILE_CONNECT_DISTANCE = 1.2f;

    private bool isMouseCasting = false;

    void Start()
    {
        gameManager = GameManager.instance;
    }

	void Update () 
    {
        // Don't cast if the tutorial screen is showing.
        if (tutorialScreen != null && tutorialScreen.tutorialPanel.enabled)
            return;

        // Don't move if the word input screen is visible.
        if (gameManager != null && gameManager.wordInput.visible)
            return;

        // Don't cast if we are looking at the physician screen.
        if (gameManager.physLockScreen != null && gameManager.physLockScreen.visible)
            return;

        bool isMouse = Input.GetMouseButton(0);

        /////////////////////////////////////////////////////
        // Events        

        if (isMouse && !isMouseCasting)
        {
            OnSelectionBegin();
            isMouseCasting = true;
        }
        if (!isMouse && isMouseCasting)
        {
            OnSelectionEnd();
            isMouseCasting = false;
            return;
        }

        if (Input.touchCount == 1 || isMouseCasting)
        {

            int layer = 1 << 8;
            Ray ray = Input.touchCount == 1 ? GetTouchRay() : GetMouseRay();

            RaycastHit[] hits = Physics.RaycastAll(ray.origin, ray.direction, 100f, layer);

            /////////////////////////////////////////////////////
            // Ray Processing

            if (hits.Length > 0)
            {
                foreach (RaycastHit hit in hits)
                {
                    GameTile tile = hit.collider.gameObject.GetComponent<GameTile>();

                    if (tile == null)
                        continue;

                    List<GameTile> currentSelection = gameManager.tileSelection.GetCurrentSelection();

                    // bool isFirstWord = tile.isEmpty && !gameManager.gridMemory.AnyWordsPlacedYet();
                    bool isTileWithinSelection = currentSelection.Contains(tile);
                    bool isFirstSelection = currentSelection.Count == 0;
                    bool directionDown = false;
                    bool directionRight = false;


                    if (!isFirstSelection)
                    {
                        directionDown = currentSelection[currentSelection.Count - 1].coordinate.y < tile.coordinate.y;
                        directionRight = currentSelection[currentSelection.Count - 1].coordinate.x < tile.coordinate.x;
                    }

                    if (!isTileWithinSelection && IsAdjacent(gameManager.tileSelection.last, tile))
                    {
                        // We should only be allowed to place words to the right or downward.
                        if (!isFirstSelection && !(directionDown || directionRight))
                            return;

                        gameManager.tileSelection.Add(tile);
                    }
                }
            }

            if (Input.touchCount == 1 && Input.touches[0].phase == TouchPhase.Ended && gameManager.tileSelection.selectedTileCount > 1)
            {
                if (gameManager.gridMemory.AnyWordsPlacedYet() && !gameManager.tileSelection.ContainsFilledTile())
                {
                    gameManager.tileSelection.ClearSelection();
                }
                else
                {
                    gameManager.wordInput.Show(gameManager.tileSelection.selectedTileCount);
                }
            }
            else if (Input.touchCount == 1 && Input.touches[0].phase == TouchPhase.Ended && gameManager.tileSelection.selectedTileCount == 1)
                gameManager.tileSelection.ClearSelection();


            /////////////////////////////////////////////////////
            // Debug

            // This will draw a ray to show the actual cast from the camera to the tiles.
            if (gameManager.debugMode)
            {
                Debug.DrawRay(ray.origin, ray.direction * 10f);
            }
        }
	}

    /// <summary>
    /// Executed when the player begins a selection.
    /// </summary>
    private void OnSelectionBegin()
    {
        // Unused
    }

    /// <summary>
    /// Executed when the player ends a selection.
    /// </summary>
    private void OnSelectionEnd()
    {
        // We do not allow a selection lower than 2 tiles.
        if(gameManager.tileSelection.selectedTileCount == 1)
        {
            gameManager.tileSelection.ClearSelection();
        }

        // Valid selection
        if(gameManager.tileSelection.selectedTileCount > 1)
        {
            if (gameManager.gridMemory.AnyWordsPlacedYet() && !gameManager.tileSelection.ContainsFilledTile())
            {
                gameManager.tileSelection.ClearSelection();
            }
            else
            {
                gameManager.wordInput.Show(gameManager.tileSelection.selectedTileCount);
            }
        }
    }

    /// <summary>
    /// Retrieves the position of the touch as a ray.
    /// Returns a blank ray if there are no touches.
    /// </summary>
    /// <returns>The position of the touch as a ray.</returns>
    private Ray GetTouchRay()
    {
        Touch touch = Input.touches[0];

        float x = touch.position.x;
        float y = touch.position.y;
        // Vector3 screenPoint = new Vector3(0, 0, 100f);

        return GetComponent<Camera>().ScreenPointToRay(new Vector3(x, y, 10f));
    }

    /// <summary>
    /// Retrieves the position of the mouse as a ray.
    /// </summary>
    /// <returns>The position of the mouse as a ray.</returns>
    private Ray GetMouseRay()
    {
        float x = Input.mousePosition.x;
        float y = Input.mousePosition.y;

        return GetComponent<Camera>().ScreenPointToRay(new Vector3(x, y, 10f));
    }
    
    /// <summary>
    /// Checks if GameTile prev is adjacent to GameTile next.
    /// </summary>
    /// <param name="prev">First GameTile</param>
    /// <param name="next">Second GameTile</param>
    /// <returns>TRUE if adjacent.</returns>
    private bool IsAdjacent(GameTile prev, GameTile next)
    {
        if (prev != null)
        {
            return Vector3.Distance(prev.transform.position, next.transform.position) < TILE_CONNECT_DISTANCE;
        }
        else
        {
            return true;
        }
    }
}
