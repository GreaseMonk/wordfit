﻿using UnityEngine;
using System.Collections;
using System;

public class CompletionBar : MonoBehaviour 
{
    [Range(0.1f, 1f)]
    public float requiredCompletion = 0.4f;

    public UILabel label;
    public UIFilledSprite background;

    private GameManager gameManager;

    private void Start()
    {
        gameManager = GameManager.instance;
        Refresh();
    }

    public void Refresh()
    {
        UpdateCompletionBar(CalculateCompletion());
    }

    private void UpdateCompletionBar(float completion)
    {
        background.fillAmount = completion;
        int completionPercent = Convert.ToInt32(completion * 100);
        completionPercent = Mathf.Clamp(completionPercent, 0, 100);
        label.text = string.Format("Voltooid: {0}%", completionPercent);
    }

    private float CalculateCompletion()
    {
        if (gameManager == null || gameManager.gridMemory == null || gameManager.gridMemory.gameTiles == null || gameManager.gridMemory.gameTiles.Count == 0)
        {
            return 0f;
        }

        int totalTiles = gameManager.gridMemory.gameTiles.Count;
        int filledTiles = 0;

        for (int i = 0; i < totalTiles; i++)
        {
            if (!gameManager.gridMemory.gameTiles[i].isEmpty)
            {
                filledTiles++;
            }
        }

        if (filledTiles == 0)
        {
            
            return 0f;
        }

        float filledTilesPercentage = (float)filledTiles / (float)totalTiles;
        float completion = filledTilesPercentage / requiredCompletion;

        return completion;
    }

    public bool isLevelCompleted
    {
        get
        {
            return CalculateCompletion() >= 1f;
        }
        
    }
}
