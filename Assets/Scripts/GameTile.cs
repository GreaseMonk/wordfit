﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameTile : MonoBehaviour 
{
    public GameObject cube;
    public GameObject letter;

    private TextMesh displayedLetter;
    private Material displayedMaterial;

    public ParticleSystem treasureParticles;

    public Material treasureMaterial;
    public Material normalMaterial;
    public Material filledMaterial;

    public Color defaultColor, permanentColor;

    public char character
    {
        get
        {
            if (displayedLetter.text.Length > 0)
                return displayedLetter.text[0];
            else return ' ';
        }
    }

    public GridCoordinate coordinate;

    private GameManager gameManager;

    public GameTile up
    {
        get
        {
            GameTile retVal;
            
            if (coordinate.y == 0)
                return null;

            retVal = gameManager.gridMemory.GetTileAt(new GridCoordinate(coordinate.x, coordinate.y - 1));

            return retVal;
        }
    }
    public GameTile down
    {
        get
        {
            GameTile retVal;

            if (coordinate.y == 0)
                return null;

            retVal = gameManager.gridMemory.GetTileAt(new GridCoordinate(coordinate.x, coordinate.y + 1));

            return retVal;
        }
    }
    public GameTile left
    {
        get
        {
            GameTile retVal;

            if (coordinate.y == 0)
                return null;

            retVal = gameManager.gridMemory.GetTileAt(new GridCoordinate(coordinate.x - 1, coordinate.y));

            return retVal;
        }
    }
    public GameTile right
    {
        get
        {
            GameTile retVal;

            if (coordinate.y == 0)
                return null;

            retVal = gameManager.gridMemory.GetTileAt(new GridCoordinate(coordinate.x + 1, coordinate.y));

            return retVal;
        }
    }

    public Word horizontalWord
    {
        get
        {
            string completeWord = string.Empty;
            GameTile currentTile = this;
            GameTile mostLeft, mostRight;
            List<GameTile> tiles = new List<GameTile>();
            
            // Get the left most tile that is not empty from this tile.
            while(currentTile.left != null && !currentTile.left.isEmpty)
                currentTile = currentTile.left;

            mostLeft = currentTile;
            tiles.Add(currentTile);
            completeWord += currentTile.character;

            // Work our way right to the most right end of the word.
            while (currentTile.right != null && !currentTile.right.isEmpty)
            {
                currentTile = currentTile.right;

                tiles.Add(currentTile);
                completeWord += currentTile.character;                
            }

            mostRight = currentTile;

			if(mostLeft == null || mostRight == null)
				return null;

            return new Word(completeWord, mostLeft.coordinate, mostRight.coordinate, tiles);
        }
    }
    public Word verticalWord
    {
        get
        {
            string completeWord = string.Empty;
            GameTile currentTile = this;
            GameTile mostTop, mostBottom;
            List<GameTile> tiles = new List<GameTile>();

            // Get the top most tile that is not empty from this tile.
            while (currentTile.up != null && !currentTile.up.isEmpty)
                currentTile = currentTile.up;

            mostTop = currentTile;
            tiles.Add(currentTile);
            completeWord += currentTile.character;

            // Work our way down to the most right of the word.
            while (currentTile.down != null && !currentTile.down.isEmpty)
            {
                currentTile = currentTile.down;

                tiles.Add(currentTile);
                completeWord += currentTile.character;
            }

            mostBottom = currentTile;

			if(mostTop == null || mostBottom == null)
				return null;

            return new Word(completeWord, mostTop.coordinate, mostBottom.coordinate, tiles);
        }
    }

    public bool adjacentToAnything
    {
        get
        {
            if (left != null && !left.isEmpty && !gameManager.tileSelection.Contains(left)   ||
                right != null && !right.isEmpty && !gameManager.tileSelection.Contains(right) ||
                up != null && !up.isEmpty && !gameManager.tileSelection.Contains(up) ||
                down != null && !down.isEmpty && !gameManager.tileSelection.Contains(down))
                return true;

            return false;
        }
    }

    public bool isEmpty
    {
        get
        {
            return char.IsWhiteSpace(character) || character == ' ';
        }
    }

    public bool isTreasure = false;
    public bool treasureClaimed = false;
    public bool isPermanent = false;
    public bool isColorBlinking = false;

    private void Start()
    {
        gameManager = GameManager.instance;

        displayedLetter = letter.GetComponent<TextMesh>();
        displayedMaterial = cube.GetComponent<Renderer>().material;

        defaultColor = new Color(1f, 1f, 1f, 0.35f);
        permanentColor = new Color(1f, 1f, 1f, 1f);

        SetCharacter(string.Empty);
        SetFaded();
    }

	public void SetCharacter(string newLetter, bool isPermanent = false)
    {
        if (!isEmpty && this.isPermanent)
            return;

        displayedLetter.text = newLetter;

        // We will only set it to permanent once.
        if(!this.isPermanent)
            this.isPermanent = isPermanent;
    }

    public void SetCharacter(char newLetter, bool isPermanent = false)
    {
        if (!isEmpty && this.isPermanent)
        {
            Debug.Log("This tile is already marked permanent. The letter will not be changed.");
            return;
        }

        displayedLetter.text = newLetter.ToString();

        // We will only set it to permanent once.
        if (!this.isPermanent)
            this.isPermanent = isPermanent;
    }

    public void SetMaterial(Material newMaterial)
    {
        displayedMaterial = newMaterial;
        cube.GetComponent<Renderer>().material = displayedMaterial;
    }

    public void SetInvisible()
    {
        SetAlpha(0f);
    }

    public void SetFaded()
    {
        if(!isColorBlinking)
        {
            if(isPermanent)
                SetAlpha(permanentColor.a);
            else
                SetAlpha(defaultColor.a);
        }
    }

    public void SetAlpha(float newAlpha)
    {
        Color newColor = displayedMaterial.color;
        newColor.a = Mathf.Clamp(newAlpha, 0f, 1f);

        displayedMaterial.SetColor("_Color", newColor);
    }

    public void BlinkColor(Color color)
    {
        if (isColorBlinking)
            StopCoroutine("BlinkColorRoutine");
        StartCoroutine(BlinkColorRoutine(color));
    }

    private IEnumerator BlinkColorRoutine(Color color)
    {
        isColorBlinking = true;
        float startTime = Time.time;
        float duration = 2f;
        float currentTime;

        displayedMaterial.SetColor("_Color", color);

        while ((currentTime = (Time.time - startTime)) < duration)
        {
            float val = Mathf.Sin(currentTime * Mathf.PI);
            SetAlpha(Mathf.Clamp01(val * val));
            yield return new WaitForSeconds(0.05f);
        }


        displayedMaterial.SetColor("_Color", isPermanent ? permanentColor : defaultColor);
        SetFaded();
        isColorBlinking = false;
    }

    /// <summary>
    /// Turns this tile into a bountiful treasure!
    /// </summary>
    public void CreateTreasure()
    {
        isTreasure = true;
        cube.GetComponent<Renderer>().material = treasureMaterial;
        treasureParticles.Play();
    }

    public void GlowYellow(bool state)
    {
        if (state)
        {
            displayedMaterial.SetColor("_Color", Color.yellow);
        }
        else
            displayedMaterial.SetColor("_Color", isPermanent ? permanentColor : defaultColor);
    }

    /// <summary>
    /// Set the gametile to permanent, locking the current
    /// letter on it.
    /// </summary>
    public void SetPermanent()
    {
        isPermanent = true;
    }
}
