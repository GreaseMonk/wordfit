﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Characters : MonoBehaviour 
{
    public GameManager gameManager;

    private string displayedCharacters;
    private int characterLimit = 24; // The maximum limit of generated buttons.
    private static string alphabet = "abcdefghijklmnopqrstuvwxyz";
    private string earnedCharacters = string.Empty;

    private string GetEarnedCharacters()
    {
        // TODO: Replace earned characters with persistent data.
        earnedCharacters = string.Empty;
        if(earnedCharacters == string.Empty)
        {
            for (int i = 0; i < characterLimit; i++)
            {
                int randNumber = Random.Range(0, alphabet.Length);

                earnedCharacters += alphabet[randNumber].ToString().ToUpper();
            }
        }

        return earnedCharacters;
    }

    public KeyboardKey GetKey(int id)
    {

        if (id < gameManager.keyHandler.keyboardKeys.Length && id >= 0)
        {
            return gameManager.keyHandler.keyboardKeys[id];
        }

        Debug.Log(string.Format("There is no button with id {0}", id.ToString()));
        return null;
    }

    private void HideCharacterButtons()
    {
        foreach (KeyboardKey key in gameManager.keyHandler.keyboardKeys)
        {
            key.gameObject.SetActive(false);
        }
    }

    private void ShowCharacterButtons()
    {
        foreach (KeyboardKey key in gameManager.keyHandler.keyboardKeys)
        {
            key.gameObject.SetActive(true);
        }
    }

    public void HideButton(int id)
    {
        KeyboardKey key = gameManager.keyHandler.keyboardKeys[id];

        if (key != null)
        {
            key.gameObject.SetActive(false);
        }
    }
}
