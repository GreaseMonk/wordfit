﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class LetterNode
{
    public char character;
    public List<LetterNode> childNodes;
    public bool isWord = false;
    public LetterNode parent;
    public bool isRoot
    {
        get
        {
            if (this.character == ' ')
                return true;

            return false;
        }
    }

    public LetterNode(char letter, LetterNode parent = null, bool isWord = false)
    {
        this.character = letter;
        this.isWord = isWord;
    }

    /// <summary>
    /// Search the children of this class for the given letter.
    /// </summary>
    /// <param name="letter">The letter to look for</param>
    /// <returns></returns>
    public LetterNode Search(char letter)
    {
        if (childNodes == null)
            childNodes = new List<LetterNode>();

        foreach (LetterNode node in childNodes)
        {
            if (node.character == letter)
                return node;
        }

        return null;
    }

    private string parsingString;
    public void Deserialize(LetterNode parent, string input = "")
    {
        LetterNode curLetter = parent;

        if (input != string.Empty && input != null)
        {
            parsingString = input;

        }

        while (true)
        {
            if (parsingString == null || parsingString.Length <= 1)
                return;

            parsingString = parsingString.Substring(1);

            if (parsingString[0] == '[')
            {
                Deserialize(curLetter);
            }
            else if (parsingString[0] == ']')
            {
                return;
            }
            else if (parsingString[0] == ' ') // ignore whitespaces, delete this check to enable white spaces in words
            {
                continue;
            }
            else // it must be a letter
            {
                char newChar = parsingString[0];
				bool isWord = newChar == char.ToUpper(newChar);
				curLetter = curLetter.Add(char.ToLower(newChar), isWord);
            }
        }
    }

    /// <summary>
    /// Get a string recursively containing all nodes.
    /// </summary>
    /// <returns></returns>
    public string serialized
    {
        get
        {
            string retVal = string.Empty;
            retVal += this.character;

            if (this.isWord)
                retVal = retVal.ToUpper();

            if (childNodes != null && childNodes.Count > 0)
            {
				//if (childNodes.Count > 1)
				//	retVal += "[";

                foreach (LetterNode child in childNodes)
                {

                    if (childNodes.Count > 1)
                        retVal += "[";
                    retVal += child.serialized;
                    if (childNodes.Count > 1)
                       retVal += "]";
                }

				//if (childNodes.Count > 1)
				//	retVal += "]";
            }
            return retVal;
        }
    }

    /// <summary>
    /// Check if a word exists in the tree.
    /// </summary>
    /// <param name="word"></param>
    public bool WordExists(string word)
    {
        if (word.Length == 1 && word[0] == this.character && this.isWord)
            return true;
        else
        {
            if (word.Length > 1)
            {
                string nextWord = word;

                if (!this.isRoot)
                    nextWord = word.Substring(1);

                // If there is no existing LetterNode with this character
                LetterNode newNode = Search(nextWord[0]);
                
                if (newNode != null)
                    return newNode.WordExists(nextWord);
            }

            return false;
        }
    }

    /// <summary>
    /// Builds the tree from the given word.
    /// </summary>
    /// <param name="word"></param>
    public void Build(string word)
    {
        // Make sure the list is initialized.
        if (childNodes == null)
            childNodes = new List<LetterNode>();

        if (word.Length == 0)
            return;

        string nextWord = string.Empty;

        if(word.Length > 1)
            nextWord = word.Substring(1, word.Length - 1);

        // If there is no existing LetterNode with this character, 
        // we must create a new LetterNode for this character.
        LetterNode newNode = Add(word[0], word.Length == 1);
        newNode.Build(nextWord);

        if (word.Length == 1)
            newNode.isWord = true;
    }

    /// <summary>
    /// Adds a letter to the children.
    /// </summary>
    /// <param name="letter">The char letter</param>
    /// <returns></returns>
    public LetterNode Add(char character, bool isWord = false)
    {
        LetterNode existingNode = Search(character);

        if (existingNode != null)
        {
            return existingNode;
        }
        else
        {
            LetterNode newCharacter = new LetterNode(character, this, isWord);
            childNodes.Add(newCharacter);
            //childNodes.Sort();

            return newCharacter;
        }
    }

    /// <summary>
    /// Removes the specified node from the children.
    /// </summary>
    /// <param name="node">The node to remove</param>
    public void Remove(LetterNode node)
    {
        if (childNodes.Contains(node))
            childNodes.Remove(node);
    }

}