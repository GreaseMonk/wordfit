﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class KeyHandler : MonoBehaviour 
{
    public GameManager gameManager;
    public KeyboardKey[] keyboardKeys;
    public UIEventListener checkBtnListener;
    public UIEventListener backspaceBtnListener;
    public UIEventListener backBtnListener;
    public UIEventListener physicianBackBtnListener;

    public AudioClip keyPress, keyReturn;

    /// <summary>
    /// Executed once on startup.
    /// </summary>
    void Start()
    {
        RegisterEventListeners();
        backBtnListener.onClick += BackBtn_OnClick;
        physicianBackBtnListener.onClick += BackBtn_OnClick;
        gameManager = GameManager.instance;
    }

    /// <summary>
    /// Executed each frame.
    /// </summary>
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            BackBtn_OnClick();
        }
    }

    public void BackBtn_OnClick(GameObject caller = null)
    {
        if (gameManager.wordInput.visible)
        {
            gameManager.wordInput.Cancel();
            gameManager.purchaseBuffer.Clear();
            gameManager.tileSelection.ClearSelection();

        }
        else if (gameManager.physLockScreen.visible)
        {
            gameManager.physLockScreen.Hide();
        }
        else if (gameManager.physLockScreen.physicianScreen != null &&
            gameManager.physLockScreen.physicianScreen.visible)
        {
            gameManager.physLockScreen.physicianScreen.Hide();
        }
        else
            Application.Quit();
    }

    /// <summary>
    /// Registers all event listeners to this script so that it can
    /// handle the click events.
    /// </summary>
    private void RegisterEventListeners()
    {
        int iterations = 0;

        foreach(KeyboardKey keyboardKey in keyboardKeys)
        {
            if(keyboardKey != null && keyboardKey.eventListener != null)
            {
                keyboardKey.eventListener.onClick += OnKeyboardKeyClick;
            }
            else
            {
                Debug.LogError("Null reference: Make sure all keyboardKeys are added.");
            }
            iterations++;
        }

        if(iterations != 26)
        {
            Debug.LogError(string.Format("There should be 26 iterations but only {0} were executed. Are all button event listeners attached ?", iterations));
        }

        checkBtnListener.onClick += OnCheckBtnClick;
        backspaceBtnListener.onClick += OnBackspaceBtnClick;
    }

    /// <summary>
    /// Updates the total amounts of stored characters for each button
    /// </summary>
    public void UpdateStoredKeyAmounts()
    {
        foreach (KeyboardKey keyboardKey in keyboardKeys)
        {
            char labelChar = keyboardKey.letterLabel.text[0];
            int storedAmount = gameManager.letterStorage.GetStoredLettersAmount(labelChar);
            keyboardKey.SetCount(storedAmount);
        }
    }

    /// <summary>
    /// Executed when a button was clicked / tapped.
    /// </summary>
    /// <param name="go">The tapped GameObject</param>
    private void OnKeyboardKeyClick(GameObject go)
    {
        for( int i = 0; i < keyboardKeys.Length; i++)
        {
            if (keyboardKeys[i].background.gameObject == go)
            {
                ProcessButtonClick(i);
                gameManager.PlaySound(keyPress);
                return;
            }
        }

        Debug.LogError("Could not find the associated button index.");
    }

    /// <summary>
    /// Adds a character to the input field depending on which
    /// button has been clicked / tapped.
    /// </summary>
    /// <param name="id"></param>
    private void ProcessButtonClick(int id)
    {
        // The amount of chars needs to match the
        // selected number of tiles.
        if (gameManager.wordInput.remainingLetters <= 0)
        {
            Debug.Log("The amount of chars needs to match the selected number of tiles.");
            return;
        }

        char letter = GetAssociatedCharacter(id)[0];
        
        if (gameManager.debugMode)
            Debug.Log("Letter pressed: " + letter);

        int storedCharacters = gameManager.letterStorage.GetStoredLettersAmount(letter);

        if (storedCharacters <= 0)
        {
            // We add this letter to the purchases list if there are
            // enough credits.
            if(gameManager.purchaseBuffer.Add(letter))
            {
                gameManager.wordInput.UpdatePurchases();
                Debug.Log(string.Format("added {0} to the purchases buffer", letter));
            }
            else
            {
                string msg = string.Format("Er zijn {0} over van de letter {1}", storedCharacters, letter);

                gameManager.wordInput.ShowMessage(msg);
                Debug.Log(msg);

                return;
            }            
        }

        gameManager.wordInput.Add(letter);

        //Debug.Log(string.Format("Added {0} to the input field.", associatedCharacter));
    }

    /// <summary>
    /// Executed when the check button was clicked / tapped
    /// </summary>
    /// <param name="go"></param>
    private void OnCheckBtnClick(GameObject go)
    {
        gameManager.CheckCurrentInput();
        gameManager.PlaySound(keyPress);
    }

    /// <summary>
    /// Executed when the backspace button is pressed
    /// </summary>
    /// <param name="go"></param>
    private void OnBackspaceBtnClick(GameObject go)
    {
        gameManager.wordInput.RemoveLast();
        gameManager.PlaySound(keyReturn);
    }

    /// <summary>
    /// Trades a number for the correct lowercase character
    /// </summary>
    /// <param name="index">A number from zero to 25</param>
    /// <returns>The associated character of the alphabet</returns>
    private string GetAssociatedCharacter(int index)
    {
        return keyboardKeys[index].letterLabel.text;
    }
}
