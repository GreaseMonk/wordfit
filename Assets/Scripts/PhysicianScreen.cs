﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Contains code on which the Physician Screen works.
/// </summary>
public class PhysicianScreen : MonoBehaviour 
{
    public UIEventListener doneBtn;
    public UIEventListener minusBtn;
    public UIEventListener plusBtn;
    public UIEventListener resetBtn;

    public UILabel todayStepsLabel, totalStepsLabel;

    public UILabel mondayLabel, tuesdayLabel, wednesdayLabel, 
        thursdayLabel, fridayLabel, saturdayLabel, sundayLabel;

    public GameObject backBtn;

    private GameManager gameManager;

	void Start () 
    {
        gameManager = GameManager.instance;

        doneBtn.onClick += OnClick_DoneBtn;
        minusBtn.onClick += OnClick_MinusBtn;
        plusBtn.onClick += OnClick_PlusBtn;
        resetBtn.onClick += OnClick_ResetBtn;
	}

    public void Show()
    {
        UpdateLabels();
        backBtn.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
        backBtn.SetActive(false);
    }

    private void UpdateLabels()
    {
        if (gameManager == null)
            gameManager = GameManager.instance;

        todayStepsLabel.text = string.Format("Vandaag {0} stappen", gameManager.health.GetTodaySteps());
        totalStepsLabel.text = string.Format("Totaal {0} stappen", gameManager.health.GetTotalSteps());


    }

    private void OnClick_DoneBtn(GameObject caller)
    {
        gameManager.health.SetTargetSteps(WeekDay.MONDAY, Convert.ToInt32(mondayLabel.text));
        gameManager.health.SetTargetSteps(WeekDay.TUESDAY, Convert.ToInt32(tuesdayLabel.text));
        gameManager.health.SetTargetSteps(WeekDay.WEDNESDAY, Convert.ToInt32(wednesdayLabel.text));
        gameManager.health.SetTargetSteps(WeekDay.THURSDAY, Convert.ToInt32(thursdayLabel.text));
        gameManager.health.SetTargetSteps(WeekDay.FRIDAY, Convert.ToInt32(fridayLabel.text));
        gameManager.health.SetTargetSteps(WeekDay.SATURDAY, Convert.ToInt32(saturdayLabel.text));
        gameManager.health.SetTargetSteps(WeekDay.SUNDAY, Convert.ToInt32(sundayLabel.text));

        Hide();
    }

    private void OnClick_MinusBtn(GameObject caller)
    {
        gameManager.health.SubstractSteps(100);
        UpdateLabels();
    }

    private void OnClick_PlusBtn(GameObject caller)
    {
        gameManager.health.AddSteps(100);
        UpdateLabels();
    }

    private void OnClick_ResetBtn(GameObject caller)
    {
        gameManager.health.SetSteps(0);
        UpdateLabels();
    }

    public bool visible
    {
        get { return gameObject.activeSelf; }
    }
}
