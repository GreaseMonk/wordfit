﻿using UnityEngine;
using System.Collections;

public class CameraSwipeDrag : MonoBehaviour 
{
    private Vector3 startPosition = Vector3.zero;
    public Vector3 minPosition = Vector3.zero;
    public Vector3 maxPosition = Vector3.zero;
    public float zoom = -10f;

    private Vector3 hitPosition;
    private Vector3 targetPosition;
    private Vector3 direction;

    private GameManager gameManager;

	// Use this for initialization
	void Start () 
    {
        startPosition = Camera.main.transform.position;
        gameManager = GameManager.instance;
    }
	
	// Update is called once per frame
	void Update () 
    {
        // Don't move if the word input screen is visible.
        if (gameManager != null && gameManager.wordInput.visible)
            return;

        if(Input.touchCount == 1)
        {
            Touch touch = Input.touches[0];

            if (touch.phase == TouchPhase.Began)
                hitPosition = touch.position;
            if (touch.phase == TouchPhase.Moved)
            {
                Vector3 fingerDisplacement = startPosition + (Vector3)(touch.position - (Vector2)hitPosition);
                direction = fingerDisplacement * 0.05f;
                targetPosition = Camera.main.transform.position + direction;
                targetPosition = new Vector3(targetPosition.x, targetPosition.y, zoom);

                gameManager.tileSelection.ClearSelection();
            }
        }

        // Zoom function
        /*
        if (Input.touchCount > 1)
            ProcessZoom();
        */

        float distanceLeft = Vector3.Distance(Camera.main.transform.position, targetPosition) * 0.5f;
        targetPosition = new Vector3(
            Mathf.Clamp(targetPosition.x, minPosition.x, maxPosition.x),
            Mathf.Clamp(targetPosition.y, minPosition.y, maxPosition.y),
            Mathf.Clamp(targetPosition.z, minPosition.z, maxPosition.z));
        Vector3 lerpedDisplacement = Vector3.Lerp(Camera.main.transform.position, targetPosition, distanceLeft * Time.deltaTime);

        

        Camera.main.transform.position = lerpedDisplacement;
	}

    private void ProcessZoom()
    {
        zoom = 5 - Vector2.Distance(Input.touches[0].position, Input.touches[1].position) * 0.05f;
        targetPosition = new Vector3(targetPosition.x, targetPosition.y, zoom);
    }
}
