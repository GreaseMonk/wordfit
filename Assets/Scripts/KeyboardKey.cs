﻿using UnityEngine;
using System.Collections;

public class KeyboardKey : MonoBehaviour 
{
    public UISprite background;
    public UILabel letterLabel;
    public UILabel countLabel;

    public Color disabledColor;
    public Color enabledColor;

    public UIEventListener eventListener;

    public bool disabled = false;

    public void SetCount(int count)
    {

        if (countLabel == null)
            return;

        countLabel.text = count.ToString();

        if (count > 99)
            countLabel.text = "99+";

        if (count <= 0)
            Disable();
        else
            Enable();
    }

    public void SetLetter(string newLetter)
    {
        if(letterLabel != null)
            letterLabel.text = newLetter;
    }

    public void Disable()
    {
        disabled = true;
        background.GetComponent<UIButtonColor>().enabled = false;
        background.GetComponent<UISlicedSprite>().color = disabledColor;
        background.color = disabledColor;
        letterLabel.color = disabledColor;
        countLabel.color = disabledColor;
    }

    public void Enable()
    {
        disabled = false;
        background.GetComponent<UIButtonColor>().enabled = true;
        background.GetComponent<UISlicedSprite>().color = enabledColor;
        background.color = enabledColor;
        letterLabel.color = Color.white;
        countLabel.color = Color.white;
    }
}
