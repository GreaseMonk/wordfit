﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;

public class WordInput : MonoBehaviour 
{
    private GameManager gameManager;

    public UILabel counterLabel;
    public UILabel titleLabel;
    public GameObject screen;
    public GameObject keyboard;
    public GridMemory memory;
    public LetterStorage letterStorage;
    public KeyHandler keyHandler;
    public UILabel purchasesLabel;

    private int wordLength = 0;
    private bool isTitleLabelResetting = false;

    private void Start()
    {
        gameManager = GameManager.instance;
        memory = gameManager.gridMemory;
        keyHandler = gameManager.keyHandler;
        letterStorage = gameManager.letterStorage;
        UpdatePurchases();
    }

    /// <summary>
    /// Blanks out all labels.
    /// </summary>
    private void ResetLabels()
    {
        titleLabel.text = string.Empty;
        counterLabel.text = string.Empty;
        UpdatePurchases();
    }

    /// <summary>
    /// Cancels the word input by resetting and hiding the screen.
    /// </summary>
    public void Cancel()
    {
        ResetLabels();
        RemoveAll();
        Hide();
        gameManager.zoomHandler.Reset();
    }

    /// <summary>
    /// Retrieves the current word input on the screen.
    /// </summary>
    /// <returns></returns>
    public string Get()
    {
        return gameManager.tileSelection.GetCurrentWord().text;
    }

    public void Add(char letter)
    {
        GameTile targetTile = gameManager.tileSelection.GetNextEmptyTile();

        if(targetTile != null && letterStorage.GetStoredLettersAmount(letter) > 0)
        {
            letterStorage.Substract(letter);
            targetTile.SetCharacter(letter);

            keyHandler.UpdateStoredKeyAmounts();
            UpdateCounter();
            UpdateZoomPosition();
        }
    }

    private void UpdateZoomPosition()
    {
        Word currentWord = gameManager.tileSelection.GetCurrentWord();
        string word = TrimWord(currentWord.text);
        int index = Mathf.Clamp(word.Length, 0, (wordLength - 1));
        GameTile targetTile = gameManager.tileSelection.GetCurrentSelection()[index];
        gameManager.zoomHandler.ZoomTo(targetTile);
    }

    private void UpdateSelectionLetters()
    {
        string currentWord = gameManager.tileSelection.GetCurrentWord().text;
        char[] currentInput = currentWord.ToCharArray();
        for (int i = 0; i < currentWord.Length; i++)
        {
            GameTile targetTile = gameManager.tileSelection.GetCurrentSelection()[i];
            if(targetTile != null && targetTile.isEmpty)
            {
                targetTile.SetCharacter(currentInput[i].ToString());
            }
        }
    }

    /// <summary>
    /// Practically a backspace, but returns the
    /// letter back into the the storage.
    /// </summary>
    public void RemoveLast()
    {
        Word currentWord = gameManager.tileSelection.GetCurrentWord();

        if (currentWord.tiles.Count == 0)
            return;

        string currentText = currentWord.text;
        if (currentText.Length <= 0)
            return;

        GameTile targetTile = null;

        // Find the last tile in the current selection that is 
        // not permanent nor empty.
        for (int i = 0; i < currentWord.tiles.Count; i++ )
        {

            if (currentWord.tiles[i] != null && !currentWord.tiles[i].isPermanent && !currentWord.tiles[i].isEmpty)
            {
                targetTile = currentWord.tiles[i];
            }
            else if (currentWord.tiles[i].isEmpty)
                break;
        }

        // Only return letters that were not previously filled.
        if (targetTile != null)
        {
            char removedChar = targetTile.character;

            targetTile.SetCharacter(string.Empty);
            letterStorage.Add(removedChar);
            UpdateCounter();
            UpdateZoomPosition();

            // Update the keyboard's displayed letter amounts.
            keyHandler.UpdateStoredKeyAmounts();
        }
    }

    /// <summary>
    /// Cycles through the current input and only return
    /// the (temporary) buffered letters that would otherwise
    /// be purchased instead.
    /// </summary>
    public void RemovePurchasedLetters()
    {
        if(gameManager.purchaseBuffer == null)
            return;

        List<char> currentLetters = gameManager.purchaseBuffer.GetCurrentLetters();
        Word currentSelectionWord = gameManager.tileSelection.GetCurrentWord();

        if (currentLetters != null && currentLetters.Count > 0)
        {
            for (int i = 0; i < currentSelectionWord.text.Length; i++ )
            {
                GameTile currentTile = currentSelectionWord.tiles[i];
                if (currentTile.isPermanent)
                    continue;

                char currentLetter = currentSelectionWord.text[i];

                // If the buffer doesnt have the letter, do nothing.
                if (currentLetters.Contains(currentLetter))
                {
                    gameManager.purchaseBuffer.Remove(currentLetter);
                    currentSelectionWord.tiles[i].SetCharacter(string.Empty);
                }
            }
        }
    }

    /// <summary>
    /// Removes all non-permanent letters from the grid.
    /// </summary>
    public void RemoveAll()
    {
        Word currentWord = gameManager.tileSelection.GetCurrentWord();

        if (currentWord.tiles.Count == 0)
            return;

        // First remove all buffered purchases.
        RemovePurchasedLetters();

        for (int i = 0; i < currentWord.tiles.Count; i++)
        {
            GameTile currentTile = currentWord.tiles[i];

            if (currentTile.isPermanent)
                continue;


            char removedLetter = currentTile.character;
            currentTile.SetCharacter(string.Empty);

            // Return the placed letter to the storage.
            gameManager.letterStorage.Add(removedLetter);
        }

        UpdatePurchases();
    }

    /// <summary>
    /// Updates the label text that counts the remaining characters.
    /// </summary>
    private void UpdateCounter()
    {
        if (remainingLetters > 0)
            counterLabel.text = string.Format("Nog {0} letters", remainingLetters.ToString());
        else
            counterLabel.text = "Druk op 'Check'";
    }

    /// <summary>
    /// Updates the label that shows which letters will 
    /// be purchased when the typed word is placed.
    /// </summary>
    public void UpdatePurchases()
    {
        string purchaseLetters = string.Empty;
        int totalCost = 0;
        purchasesLabel.text = string.Empty;

        List<char> bufferedLetters = gameManager.purchaseBuffer.GetCurrentLetters();

        if(bufferedLetters == null)
            return;

        foreach (char bufferedLetter in bufferedLetters)
        {
            purchaseLetters += bufferedLetter + ",";
            totalCost += gameManager.purchaseBuffer.GetCost(bufferedLetter);
        }

        if (purchaseLetters.Length > 0)
        {
            // Remove the last comma from the string.
            purchaseLetters.Remove(purchaseLetters.Length - 2, 1);

            purchasesLabel.text = string.Format("De letters {0} worden gekocht voor {1} credits.", purchaseLetters, totalCost.ToString());
        }
    }

    /// <summary>
    /// Shows the word input screen.
    /// </summary>
    public void Show(int wordLength)
    {
        screen.SetActive(true);
        keyboard.SetActive(true);

        ResetLabels();

        this.wordLength = wordLength;
        UpdateCounter();

        gameManager.zoomHandler.ZoomTo(gameManager.tileSelection.GetCurrentSelection()[0]);
    }

    /// <summary>
    /// Hides the word input screen.
    /// </summary>
    public void Hide()
    {
        screen.SetActive(false);
        keyboard.SetActive(false);
        gameManager.zoomHandler.Reset();
    }

    /// <summary>
    /// Validates the current word input.
    /// </summary>
    /// <returns></returns>
    public bool Validate()
    {
        return false;
    }

    /// <summary>
    /// Shows a message on the counter.
    /// Used to show the user when a letter button is pressed,
    /// but the player has no more left.
    /// </summary>
    /// <param name="msg"></param>
    public void ShowMessage(string msg)
    {
        counterLabel.text = msg;
    }
    
    /// <summary>
    /// Shows a message in large on top of the screen for 2 seconds.
    /// </summary>
    /// <param name="msg"></param>
    public void ShowTitleMessage(string msg, bool hideAfterwards = false)
    {
        StartCoroutine(TitleLabelRoutine(msg, hideAfterwards));
    }

    /// <summary>
    /// Clears and hides the notification label (below the input) after 2 seconds.
    /// </summary>
    /// <param name="clearInputScreen"></param>
    /// <returns></returns>
    public IEnumerator TitleLabelRoutine(string msg, bool hideAfterwards = false)
    {
        if (!isTitleLabelResetting)
        {
            isTitleLabelResetting = true;
            titleLabel.text = msg;

            yield return new WaitForSeconds(2f);

            if (hideAfterwards)
                Hide();

            isTitleLabelResetting = false;
        }
    }

    private static string TrimWord(string input)
    {
        StringBuilder output = new StringBuilder(input.Length);

        for (int index = 0; index < input.Length; index++)
        {
            if (!char.IsWhiteSpace(input, index))
            {
                output.Append(input[index]);
            }
            else
                return output.ToString();
        }

        return output.ToString();
    }

    /// <summary>
    /// Retrieves the amount of letters remaining.
    /// </summary>
    public int remainingLetters
    {
        get
        {
            Word currentWord = gameManager.tileSelection.GetCurrentWord();
            string word = TrimWord(currentWord.text);

            int retVal = wordLength - word.Length;

            if(gameManager.debugMode)
            {
                Debug.Log(string.Format("Letters remaining: {0} | Current word: {1}", retVal, word));
            }

            /*Debug.Log("Selection word length: " + wordLength.ToString());
            Debug.Log("Current input word length: " + currentWord.text.Length.ToString());
            Debug.Log(currentWord.text);
            Debug.Log("Remaining letters: " + retVal.ToString());*/
            return retVal;
        }
    }

    /// <summary>
    /// Retrieves the visibility state of the word input screen.
    /// </summary>
    public bool visible
    {
        get
        {
            return screen.activeSelf;
        }
    }
}
