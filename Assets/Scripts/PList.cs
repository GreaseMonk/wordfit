﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

public class PList : Dictionary<string, string>
{
    public PList()
    {
    }

    public PList(string file)
    {
        Load(file);
    }

    public void Load(string file)
    {
        Clear();

        XDocument doc = XDocument.Load(file);
        XElement plist = doc.Element("plist");
        XElement dict = plist.Element("dict");

        var dictElements = dict.Elements();
        Parse(this, dictElements);
    }

    private void Parse(PList dict, IEnumerable<XElement> elements)
    {
        for (int i = 0; i < elements.Count(); i += 2)
        {
            XElement key = elements.ElementAt(i);
            XElement val = elements.ElementAt(i + 1);

            dict[key.Value] = ParseValue(val);
        }
    }

    private System.String ParseValue(XElement val)
    {
        switch (val.Name.ToString())
        {
            case "string":
                return val.Value;
            default:
                throw new ArgumentException("Unsupported");
        }
    }
}
