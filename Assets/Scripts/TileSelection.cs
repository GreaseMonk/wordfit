﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TileSelection : MonoBehaviour 
{
    private List<GameTile> selectedTiles;
    private List<GameTile> alreadyFilledTiles;
    private GameManager gameManager;

    /// <summary>
    /// Initialize the class.
    /// </summary>
    public void Start()
    {
        selectedTiles = new List<GameTile>();
        alreadyFilledTiles = new List<GameTile>();
        gameManager = GameManager.instance;
    }

    /// <summary>
    /// Adds a tile to the current selection.
    /// </summary>
    /// <param name="tile"></param>
    public void Add(GameTile tile)
    {
        if (selectedTiles.Contains(tile))
            return;

        // The selected tile is not in alignment with the first selected tile.
        if (selectedTiles.Count > 0 && !IsAligned(tile))
        {
            //Debug.Log(string.Format("Tile at {0} is not aligned with origin tile at {1}", tile.transform.position, selectedTiles[0].transform.position));
            return;
        }

        //Debug.Log(string.Format("Tile at {0} added to the selection.", tile.transform.position));

        if (!tile.isEmpty)
            alreadyFilledTiles.Add(tile);

        tile.SetAlpha(0.7f);
        selectedTiles.Add(tile);
    }

    /// <summary>
    /// Removes a tile from the selection.
    /// </summary>
    /// <param name="tile">the tile to remove.</param>
    public void Remove(GameTile tile)
    {
        foreach(GameTile selectedTile in selectedTiles)
        {
            if (selectedTile == tile)
            {
                selectedTiles.Remove(selectedTile);
                tile.cube.GetComponent<Renderer>().material.SetColor("_Color", tile.defaultColor);
            }
        }
        foreach(GameTile selectedTile in alreadyFilledTiles)
        {
            if(selectedTile == tile)
            {
                alreadyFilledTiles.Remove(selectedTile);
            }
        }
    }

    public List<GameTile> GetCurrentSelection()
    {
        return selectedTiles;
    }

    public bool ContainsFilledTile()
    {
        bool hasFilledTile = false;

        foreach(GameTile tile in selectedTiles)
        {
            if (!tile.isEmpty)
                hasFilledTile = true;
        }

        return hasFilledTile;
    }

    /// <summary>
    /// Checks if the current selection of tiles
    /// contains the given tile.
    /// </summary>
    /// <param name="tile">The tile to check for.</param>
    /// <returns>If the tile is contained in the current selection</returns>
    public bool Contains(GameTile tile)
    {
        return selectedTiles.Contains(tile);
    }

    /// <summary>
    /// Clears the current selection.
    /// </summary>
    public void ClearSelection()
    {
        /*foreach (GameTile tile in gameManager.gridMemory.gameTiles)
        {
            if (tile.isEmpty)
            {
                tile.SetFaded();
            }
        }*/
        foreach(GameTile tile in selectedTiles)
        {
            tile.SetFaded();

            // Continue iteration if a character has been filled in.
            //if (tile.character != ' ' || !char.IsWhiteSpace(tile.character))
            //    continue;
        }

        selectedTiles = new List<GameTile>();
        alreadyFilledTiles = new List<GameTile>();
    }

    /// <summary>
    /// Check if the given tile is in alignment with
    /// the first selected gametile.
    /// </summary>
    /// <param name="tile"></param>
    private bool IsAligned(GameTile tile)
    {
        if (selectedTiles == null || selectedTiles.Count == 0)
            return false;

        Vector3 origin = selectedTiles[0].transform.position;

        if(selectedTiles.Count == 1)
        {
            if (tile.transform.position.x == origin.x)
                return true;
            if (tile.transform.position.y == origin.y)
                return true;
        }
        else if(selectedTiles.Count > 1)
        {
            Vector3 nextOrigin = selectedTiles[1].transform.position;

            if (tile.transform.position.x == origin.x && tile.transform.position.x == nextOrigin.x)
                return true;
            if (tile.transform.position.y == origin.y && tile.transform.position.y == nextOrigin.y)
                return true;
        }

        return false;
    }

    /// <summary>
    /// Get the amount of selected tiles.
    /// This is used so that the tile selection process
    /// won't continue if the user has not selected any
    /// tiles.
    /// </summary>
    public int selectedTileCount
    {
        get
        {
            return selectedTiles.Count;
        }
    }

    /// <summary>
    /// Fills in a word onto the tiles.
    /// </summary>
    /// <param name="word">The word to fill onto the tiles.</param>
    public void FillWord(string word)
    {
        if (word.Length != selectedTiles.Count)
        {
            Debug.Log("Amount of tiles does not correspond with amount of characters in the given word.");
            return;
        }

        for (int i = 0; i < selectedTiles.Count; i++)
        {
            if(!alreadyFilledTiles.Contains(selectedTiles[i]))
                selectedTiles[i].SetCharacter(word[i].ToString());
        }
    }

    public void RemoveWord()
    {
        for (int i = 0; i < selectedTiles.Count; i++)
        {
            if(!alreadyFilledTiles.Contains(selectedTiles[i]))
                selectedTiles[i].SetCharacter(string.Empty);
        }
    }

    public GameTile first
    {
        get
        {
            if (selectedTiles != null && selectedTiles.Count > 0)
                return selectedTiles[0];

            return null;
        }
    }

    public GameTile last
    {
        get
        {
            if (selectedTiles != null && selectedTiles.Count > 0)
                return selectedTiles[selectedTiles.Count - 1];

            return null;
        }
    }

    /*public void Select(GridCoordinate start, GridCoordinate end)
    {
        bool isHorizontal = start.y == end.y;

        if(isHorizontal)
        {
            
        }
    }*/

    public Word GetCurrentWord()
    {
        string word = string.Empty;

        for (int i = 0; i < selectedTiles.Count; i++ )
        {
            word += selectedTiles[i].character;
        }

        if (selectedTiles.Count > 1)
            return new Word(word, selectedTiles[0].coordinate, selectedTiles[selectedTiles.Count - 1].coordinate, selectedTiles);

        return null;
    }

    public Word FinishWord()
    {
        Word word = new Word(gameManager.wordInput.Get(), selectedTiles[0].coordinate, selectedTiles[selectedTiles.Count - 1].coordinate, selectedTiles);

        ClearSelection();
        
        return word;
    }

    public GameTile GetNextEmptyTile()
    {
        foreach(GameTile tile in GetCurrentSelection())
        {
            if (tile.isEmpty)
                return tile;
        }

        return null;
    }

    public int GetSelectedTreasures()
    {
        int retVal = 0;
        foreach(GameTile tile in selectedTiles)
        {
            if (tile.isTreasure && !tile.treasureClaimed)
                retVal++;
        }

        return retVal;
    }
}
