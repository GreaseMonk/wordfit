﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum HealthStatus { VERY_LOW, LOW, HEALTHY, HIGH, TOO_HIGH }
public enum WeekDay { MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY }

public class Health : MonoBehaviour 
{
    public GameObject veryLowTorch, lowTorch, healthyTorch, highTorch, veryHighTorch;

    private Dictionary<WeekDay, int> targetSteps;

    private int todaySteps, totalSteps;
    private HealthStatus status = HealthStatus.HEALTHY;

    private void Start()
    {
        todaySteps = 0;
        totalSteps = 0;
        targetSteps = new Dictionary<WeekDay, int>();
        targetSteps.Add(WeekDay.MONDAY, 1600);
        targetSteps.Add(WeekDay.TUESDAY, 2300);
        targetSteps.Add(WeekDay.WEDNESDAY, 2300);
        targetSteps.Add(WeekDay.THURSDAY, 2300);
        targetSteps.Add(WeekDay.FRIDAY, 1800);
        targetSteps.Add(WeekDay.SATURDAY, 1200);
        targetSteps.Add(WeekDay.SUNDAY, 1000);
    }
    
    public void AddSteps(int amount)
    {
        todaySteps += amount;
        totalSteps += amount;
    }

    public void SubstractSteps(int amount)
    {
        todaySteps -= amount;
        totalSteps -= amount;

        if (totalSteps < 0)
            totalSteps = 0;
        if (todaySteps < 0)
            todaySteps = 0;
    }

    public void SetSteps(int amount)
    {
        if (amount >= 0)
            todaySteps = amount;
    }

    public int GetTodaySteps()
    {
        return todaySteps;
    }
    
    public int GetTotalSteps()
    {
        return totalSteps;
    }

    public HealthStatus GetStatus()
    {
        return status;
    }

    public void UpdateHealth()
    {
        /*int startTime = 9;
        int endTime = 20;
        int current = System.DateTime.Now.Hour;

        int idealStepsNow = */

        // TODO: calculate ideal steps at the current hour of the day.
        SetTorchActive(HealthStatus.HEALTHY);
        
    }

    public void SetTorchActive(HealthStatus status)
    {
        veryLowTorch.SetActive(false);
        lowTorch.SetActive(false);
        healthyTorch.SetActive(false);
        highTorch.SetActive(false);
        veryHighTorch.SetActive(false);

        switch (status)
        {
            case HealthStatus.VERY_LOW:
                veryLowTorch.SetActive(true);
                break;
            case HealthStatus.LOW:
                lowTorch.SetActive(true);
                break;
            case HealthStatus.HEALTHY:
                healthyTorch.SetActive(true);
                break;
            case HealthStatus.HIGH:
                highTorch.SetActive(true);
                break;
            case HealthStatus.TOO_HIGH:
                veryHighTorch.SetActive(true);
                break;
        }
    }

    public void SetTargetSteps(WeekDay day, int idealSteps)
    {
        int val;
        if (targetSteps.TryGetValue(day, out val))
        {
            targetSteps[day] = idealSteps;
        }
    }

    private KeyValuePair<WeekDay, int> GetDayKvp(WeekDay day)
    {
        foreach (KeyValuePair<WeekDay, int> val in targetSteps)
        {
            if (val.Key == day)
            {
                return val;
            }
        }
        return new KeyValuePair<WeekDay,int>();
    }
}
