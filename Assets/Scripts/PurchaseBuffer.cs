﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class PurchaseBuffer : MonoBehaviour
{
    private GameManager gameManager;
    private CreditsManager credits;
    private LetterStorage letterStorage;

    private List<char> purchaseBuffer;
    private int currentCost = 0;

    /// <summary>
    /// Executed at the start of the game.
    /// </summary>
    private void Start()
    {
        gameManager = GameManager.instance;
        credits = gameManager.credits;
        letterStorage = gameManager.letterStorage;
    }

    /// <summary>
    /// Adds a letter to the buffer for purchasing.
    /// </summary>
    /// <param name="letter"></param>
    /// <returns></returns>
    public bool Add(char letter)
    {
        // If the player does not have the credits to purchase the
        // current sum of letters, we can't continue here.
        if(credits.GetCurrent() < (currentCost + GetCost(letter)))
            return false;

        if(purchaseBuffer == null)
            purchaseBuffer = new List<char>();

        purchaseBuffer.Add(letter);
        
        return true;
    }

    /// <summary>
    /// Make the purchase. 
    /// Subtracts the credits, adds the letters to the storage.
    /// </summary>
    public void DoPurchase()
    {
        if(purchaseBuffer != null && purchaseBuffer.Count > 0)
        {
            credits.Substract(currentCost);

            foreach (char letter in purchaseBuffer)
                letterStorage.Add(letter);

            Clear();
        }
    }

    public void Remove(char letter)
    {
        if (purchaseBuffer.Contains(letter))
        {
            purchaseBuffer.Remove(letter);
            Debug.Log(string.Format("Removed {0} from the purchases buffer", letter));
        }
    }

    /// <summary>
    /// Clears the buffer / Resets the buffer.
    /// </summary>
    public void Clear()
    {
        gameManager.wordInput.RemovePurchasedLetters();
        purchaseBuffer = new List<char>();
        currentCost = 0;
    }

    /// <summary>
    /// Gets the required amount of credits for a specific letter.
    /// </summary>
    /// <param name="letter"></param>
    /// <returns></returns>
    public int GetCost(char letter)
    {
        // TODO: add different costs for all different letters.
        return 500;
    }

    /// <summary>
    /// Gets the costs for the current list of letters for purchase.
    /// </summary>
    /// <returns></returns>
    public int GetCurrentCost()
    {
        return currentCost;
    }

    /// <summary>
    /// Retrieves the current letters that are pending to be 
    /// purchased.
    /// </summary>
    /// <returns></returns>
    public List<char> GetCurrentLetters()
    {
        return purchaseBuffer;
    }
}

