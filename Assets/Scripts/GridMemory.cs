﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GridMemory : MonoBehaviour 
{
    public List<GameTile> gameTiles;
    private List<Word> placedWords;
    /*private GameManager gameManager;

    private void Start()
    {
        gameManager = GameManager.instance;
    }*/

    /// <summary>
    /// Check if any words are placed onto the grid yet.
    /// </summary>
    /// <returns>TRUE if there is a word on the grid.</returns>
    public bool AnyWordsPlacedYet()
    {
        if(placedWords == null || placedWords.Count == 0)
            return false;
        else return true;
    }

    public void AddGameTile(GameTile newTile)
    {
        if (gameTiles == null)
            gameTiles = new List<GameTile>();

        gameTiles.Add(newTile);
    }

    /// <summary>
    /// Retrieves the char that is displayed on the tile.
    /// </summary>
    /// <param name="coord">The coord of the tile to retrieve the char from</param>
    /// <returns>the char contained at the specified location</returns>
    public char GetCharAt(GridCoordinate coord)
    {
        GameTile matchedTile = null;

        foreach(GameTile tile in gameTiles)
        {
            if (tile.coordinate.x == coord.x && tile.coordinate.y == coord.y)
                matchedTile = tile;
        }

        if(matchedTile == null)
        {
            Debug.LogError("NullReference: Can't retrieve from this coordinate because it is nonexistent (outside grid ?).");
            return ' ';
        }

        return matchedTile.character;
    }

    /// <summary>
    /// Retrieves the word that is positioned at the given coord.
    /// </summary>
    /// <param name="coord">The coord of any char of the word.</param>
    /// <returns>the word (struct)</returns>
    /*public Word GetWordAt(GridCoordinate coord)
    {
        return gameManager.gridMemory.GetTileAt(coord)
    }*/

    /// <summary>
    /// Retrieves the tile at the given coordinate
    /// </summary>
    /// <param name="coord">The coordinate to check</param>
    /// <returns></returns>
    public GameTile GetTileAt(GridCoordinate coord)
    {
        foreach(GameTile tile in gameTiles)
        {
            if (tile.coordinate.x == coord.x && tile.coordinate.y == coord.y)
                return tile;
        }

        return null;
    }

    public void AddWord(Word newWord)
    {
        if (placedWords == null)
            placedWords = new List<Word>();


        // Mark all tiles as permanent so the player cannot change
        // the letter anymore according to scrabble rules.
        for ( int i = 0; i < newWord.tiles.Count; i++)
        {
            newWord.tiles[i].SetPermanent();
        }

        placedWords.Add(newWord);
    }
}

public class GridCoordinate : object
{
    public int x, y;

    public GridCoordinate(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public override bool Equals(System.Object obj)
    {
        // Check for null values and compare run-time types.
        if (obj == null || GetType() != obj.GetType())
            return false;

        GridCoordinate other = (GridCoordinate)obj;

        return this.x == other.x && this.y == other.y;
    }

    public override int GetHashCode()
    {
        return x ^ y;
    }
}

public class Word
{
    public string text;
    public GridCoordinate start, end;
    public List<GameTile> tiles;

    public bool isHorizontal
    {
        get
        {
            return start.y == end.y;
        }
    }

    public Word(string text, GridCoordinate start, GridCoordinate end, List<GameTile> tiles)
    {
        this.text = text;
        this.start = start;
        this.end = end;
        this.tiles = tiles;
    }

    /// <summary>
    /// Calculate if this word is at a certain coordinate on the grid
    /// </summary>
    /// <param name="coord">The coordinate to check</param>
    /// <returns>TRUE if the word is at the given coordinate</returns>
    public bool IsWordAt(GridCoordinate coord)
    {
        if (isHorizontal && coord.y != start.y)
            return false;

        if (!isHorizontal && coord.x != start.x)
            return false;

        if (isHorizontal && ((coord.y > start.y && coord.y < end.y) || (coord.y > end.y && coord.y < start.y)))
            return true;

        if (!isHorizontal && ((coord.x > start.x && coord.x < end.x) || (coord.x > end.x && coord.x < start.x)))
            return true;

        return false;
    }    
}