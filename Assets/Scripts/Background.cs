﻿using UnityEngine;
using System.Collections;

public class Background : MonoBehaviour 
{
    public Texture2D[] backgrounds;

    private void Start()
    {
        if (backgrounds == null)
            return;

        int selected = Random.Range(0, backgrounds.Length);

        GetComponent<Renderer>().material.SetTexture("_MainTex", backgrounds[selected]);
    }
}
