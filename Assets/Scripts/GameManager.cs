﻿using UnityEngine;
using System.Collections;
//using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

public class GameManager : MonoBehaviour 
{
    public KeyHandler keyHandler;
    public Characters characters;
    public WordInput wordInput;
    public TileSelection tileSelection;
    public LetterStorage letterStorage;
    public CreditsManager credits;
    public ParticleSystem rewardParticles;
    public GameObject rewardScreen;
    public GameObject completeLevelBtn;
    public UILabel rewardCounterLabel;
    public GameObject keyboardPanel;
    public GridMemory gridMemory;
    public PurchaseBuffer purchaseBuffer;
    public ZoomHandler zoomHandler;
    public TutorialScreen tutorialScreen;
    public UIEventListener helpBtnListener;
    public UIEventListener physBtnListener;
    public UIEventListener finishLevelBtnListener;
    public PhysicianLockScreen physLockScreen;
    public Health health;
    public CompletionBar completionBar;

    private LetterNode rootNode;

    public AudioClip wordCorrect, wordIncorrect;

    public bool debugMode = false;

    /// <summary>
    /// Executed on startup.
    /// Initializes the class.
    /// </summary>
    private void Start()
    {
        letterStorage = new LetterStorage();
        credits = GetComponent<CreditsManager>();
        rootNode = new LetterNode(' ');

        InitializeWordTree();
        keyHandler.UpdateStoredKeyAmounts();

        helpBtnListener.onClick += OnHelpBtnClicked;
        physBtnListener.onClick += OnPhysicianBtnClicked;
        finishLevelBtnListener.onClick += OnFinishLevelBtnClicked;

        health.SetTorchActive(HealthStatus.HEALTHY);

        UpdateCompletionHUD();
    }

    /// <summary>
    /// Initialize the library of words, adding each word
    /// to a search tree.
    /// </summary>
    private void InitializeWordTree()
    {
        TextAsset rawWords = (TextAsset)Resources.Load("words_json", typeof(TextAsset));
        StringReader reader = new StringReader(rawWords.text);

        JSON js = new JSON();
        js.serialized = reader.ReadToEnd();

        string[] words = js.ToArray<string>("words");

        foreach (string word in words)
        {
            rootNode.Build(word);
        }
    }

    /// <summary>
    /// Checks the current word input and 
    /// handles when it is correct or incorrect.
    /// </summary>
    public void CheckCurrentInput()
    {
        Word newWord = tileSelection.GetCurrentWord();
        string word = newWord.text;

        if (word.Length <= 1 || wordInput.remainingLetters > 0)
            return;

        bool wordValid = rootNode.WordExists(word);
        bool adjacencyCorrect = IsAdjacencyCorrect(newWord);
        

        if (wordValid && adjacencyCorrect)
        {
            // Notify the player of his/her success!
            wordInput.ShowTitleMessage("Correct!", true);
            // Substract any credits if the player had any
            // pending purchases of letters.
            purchaseBuffer.DoPurchase();
            // Place the word onto the grid.
            tileSelection.FillWord(word);

            foreach (GameTile gameTile in tileSelection.GetCurrentSelection())
            {
                gameTile.BlinkColor(Color.green);
                gameTile.SetMaterial(gameTile.filledMaterial);
                gameTile.treasureParticles.Stop();
                gameTile.treasureClaimed = true;
            }

            int rewardedCredits = credits.GetReward(word);
            if (rewardParticles != null)
                rewardParticles.Play();

            PlaySound(wordCorrect);
            gridMemory.AddWord(tileSelection.GetCurrentWord());

            StartCoroutine(BonusRewardCoroutine(rewardedCredits, tileSelection.GetSelectedTreasures()));
            tileSelection.ClearSelection();
        }
        else
        {
            string msg = "Probeer het opnieuw.";
            string titleMsg = "Onjuist";

            PlaySound(wordIncorrect);
            wordInput.ShowMessage(msg);
            wordInput.ShowTitleMessage(titleMsg);
            Debug.Log(msg);

            zoomHandler.Reset();
            wordInput.RemoveAll();
        }

        UpdateCompletionHUD();
    }

    /// <summary>
    /// Sets visibility of finish level button,
    /// updates progress bar for level completion.
    /// </summary>
    private void UpdateCompletionHUD()
    {
        completionBar.Refresh();

        // Set the visibility of the finish level button.
        if (completionBar.isLevelCompleted && !completeLevelBtn.activeSelf)
            completeLevelBtn.SetActive(true);
        else if (!completionBar.isLevelCompleted && completeLevelBtn.activeSelf)
            completeLevelBtn.SetActive(false);
    }

    /// <summary>
    /// Check if the given word has any intersecting characters.
    /// </summary>
    /// <param name="newWord">The word to check the adjacency of</param>
    /// <returns>TRUE if all is correct</returns>
    private bool IsAdjacencyCorrect(Word newWord)
    {
        List<Word> newWords = new List<Word>();

        tileSelection.FillWord(newWord.text);

        foreach(GameTile tile in newWord.tiles)
        {
            if (tile.adjacentToAnything)
            {
                Word horizontalWord = tile.horizontalWord;
                Word verticalWord = tile.verticalWord;

                if (horizontalWord != null)
                    Debug.Log("Horizontal: " + horizontalWord.text);
                if (verticalWord != null)
                    Debug.Log("Vertical: " + verticalWord.text);

				if(horizontalWord != null && horizontalWord.text != string.Empty)
				{
	                if (!rootNode.WordExists(horizontalWord.text))
	                {
                        foreach (GameTile wordTile in horizontalWord.tiles)
                            wordTile.BlinkColor(Color.red);

                        tileSelection.RemoveWord();
	                    return false;
	                }
	                else
	                    newWords.Add(horizontalWord);
				}
				if(verticalWord != null && verticalWord.text != string.Empty)
				{
	                if (!rootNode.WordExists(verticalWord.text))
	                {
	                    foreach (GameTile wordTile in verticalWord.tiles)
	                        wordTile.BlinkColor(Color.red);

                        tileSelection.RemoveWord();
	                    return false;
	                }
	                else
	                    newWords.Add(verticalWord);
				}
            }
        }

        // If even one word does not exist while in the process,
        // the new words will not be placed and everything will be cancelled.
        foreach (Word word in newWords)
            gridMemory.AddWord(word);

        tileSelection.RemoveWord();
        return true;
    }

    

    private IEnumerator BonusRewardCoroutine(int reward, int treasures = 0)
    {
        rewardScreen.SetActive(true);
        keyboardPanel.SetActive(false);

        float duration = 3.0f;
        float startTime = Time.time;
        float currentTime;

        int bonusReward = 0;
        int totalReward = 0;
        int treasureReward = treasures * 750;

        string counterText = string.Empty;

        while((currentTime = Time.time - startTime) < duration)
        {
            float expGrowthVal = currentTime / duration;
            expGrowthVal = Mathf.Sqrt(1 - ((expGrowthVal - 1) * (expGrowthVal - 1)));
            float currentProgress = Mathf.Clamp((expGrowthVal * expGrowthVal), 0f, 1f);
            bonusReward = (int)Mathf.Lerp(0, reward, currentProgress);

            totalReward = treasureReward + bonusReward;
            
            // Determine what the displayed string is for the counter in the middle of the screen.
            if(treasures > 0)
                counterText = string.Format("{0} (+{1} Treasure Bonus", bonusReward.ToString("f0"), treasureReward);
            else
                counterText = Mathf.RoundToInt(totalReward).ToString("f0");

            // Update the counter label text in the middle of the screen.
            rewardCounterLabel.text = counterText;

            // Update the player's current score on the top left of the screen.
            credits.SetCreditsLabel((Mathf.RoundToInt(credits.GetCurrent() + treasureReward  + bonusReward)).ToString());
            
            yield return null;
        }

        totalReward = treasureReward + reward;
        credits.SetCreditsLabel((Mathf.RoundToInt(credits.GetCurrent() + treasureReward + reward)).ToString());

        // Determine what the displayed string is for the counter in the middle of the screen.
        if (treasures > 0)
            counterText = string.Format("{0} (+{1} Treasure Bonus", bonusReward.ToString("f0"), treasureReward);
        else
            counterText = Mathf.RoundToInt(totalReward).ToString("f0");

        rewardCounterLabel.text = counterText;

        yield return new WaitForSeconds(2.0f);

        rewardScreen.SetActive(false);
        credits.Add(totalReward);
    }

    /// <summary>
    /// Plays the given sound once.
    /// </summary>
    /// <param name="clip">The clip that needs to be played.</param>
    public void PlaySound(AudioClip clip)
    {
        if (this.GetComponent<AudioSource>() == null)
            gameObject.AddComponent<AudioSource>();
        if (clip == null)
        {
            Debug.LogError("Nullreference caught on clip reference");
            return;
        }

        this.GetComponent<AudioSource>().PlayOneShot(clip);
    }

    private void OnHelpBtnClicked(GameObject caller)
    {
        if (wordInput.visible)
            return;
        if (tutorialScreen.tutorialPanel.enabled)
            return;
        if (physLockScreen.visible)
            return;

        tutorialScreen.Show();
    }

    private void OnPhysicianBtnClicked(GameObject caller)
    {
        if (!physLockScreen.visible)
            physLockScreen.Show();
    }

    public static GameManager instance
    {
        get
        {
            return FindObjectOfType(typeof(GameManager)) as GameManager;
        }
    }

    /// <summary>
    /// Executed when the Finish Level button was pressed.
    /// For now, it restarts the game.
    /// </summary>
    /// <param name="caller"></param>
    private void OnFinishLevelBtnClicked(GameObject caller)
    {
        Application.LoadLevel(Application.loadedLevel);
    }
}
