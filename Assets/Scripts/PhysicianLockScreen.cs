﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Contains code on which the Physician Lock Screen works.
/// </summary>
/// <remarks>
/// Not to be confused with Physician Screen.
/// </remarks>
public class PhysicianLockScreen : MonoBehaviour 
{
    public GameObject stretchObject;
    public GameObject lockScreen;
    public GameObject physScreen;

    public UILabel passcodeLabel;
    public UIEventListener LockOkBtnListener;
    public GameObject backBtn;

    public PhysicianScreen physicianScreen;

    private static string password = "00000";

    private void Start()
    {
        LockOkBtnListener.onClick += LockOkBtnClicked;
        
        Hide();

    }

	public void Show()
    {
        stretchObject.SetActive(true);
        lockScreen.SetActive(true);
        backBtn.SetActive(true);
    }

    public void Hide()
    {
        stretchObject.SetActive(false);
        lockScreen.SetActive(false);
        physScreen.SetActive(false);
        backBtn.SetActive(false);
    }

    private void CheckPassCode()
    {
        if(passcodeLabel.text == password)
        {
            passcodeLabel.text = string.Empty;
            UnlockPhysicianScreen();
        }
    }

    private void UnlockPhysicianScreen()
    {
        lockScreen.SetActive(false);
        physScreen.SetActive(true);

        physicianScreen.Show();
    }

    private void LockOkBtnClicked(GameObject caller)
    {
        CheckPassCode();
    }

    public bool visible
    {
        get
        {
            return stretchObject.activeSelf || lockScreen.activeSelf || physScreen.activeSelf;
        }
    }
}
