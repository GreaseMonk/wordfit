﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Contains code for the Tutorial Screen to work.
/// </summary>
public class TutorialScreen : MonoBehaviour 
{
    public UIEventListener btnListener;
    public UIPanel tutorialPanel;
    public GameObject background;

    private void Start()
    {
        tutorialPanel.enabled = true;
        btnListener.onClick += OnButtonClick_GotItBtn;
    }

    private void OnButtonClick_GotItBtn(GameObject go)
    {
        Hide();
    }

    public void Show()
    {
        tutorialPanel.enabled = true;
        background.SetActive(true);
    }

    public void Hide()
    {
        tutorialPanel.enabled = false;
        background.SetActive(false);
    }
}
