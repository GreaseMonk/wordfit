﻿using UnityEngine;
using System.Collections;

public class TileGenerator : MonoBehaviour 
{
    public GameObject[] tiles;
    public Vector2 startPosition;
    public Vector2 tileLimits;
    public Vector2 offset;
    private GameManager gameManager;

    private void Start()
    {
        gameManager = GameManager.instance;
        startPosition = new Vector2(transform.position.x, transform.position.y);
        Generate();
    }

    public void Generate()
    {
        Vector2 currentPosition = startPosition;
        for (int y = 0; y < (int)tileLimits.y; y++)
        {
            // Generate rows.

            for (int x = 0; x < (int)tileLimits.x; x++)
            {
                // Generate columns.

                GameObject newTile = Instantiate(tiles[0], currentPosition, Quaternion.identity) as GameObject;
                newTile.transform.parent = transform;

                currentPosition = new Vector2(currentPosition.x + offset.x, currentPosition.y);

                // Generate a tile coordinate so we can find the tile back
                GridCoordinate tileCoord = new GridCoordinate(x,y);

                GameTile newGameTile = newTile.GetComponent<GameTile>();
                newGameTile.coordinate = tileCoord;

                // Roll the dice if this will be a treasure.
                // Chance = 5%
                float roll = Random.Range(0f, 100f);
                if (roll <= 5f)
                    newGameTile.CreateTreasure();

                gameManager.gridMemory.AddGameTile(newGameTile);
            }

            currentPosition = new Vector2(startPosition.x, currentPosition.y - offset.y);
        }
    }
    
}
